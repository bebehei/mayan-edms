# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Aline Freitas <aline@alinefreitas.com.br>, 2016
# José Samuel Facundo da Silva <samuel.facundo@ufca.edu.br>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:29-0400\n"
"PO-Revision-Date: 2018-12-21 01:19+0000\n"
"Last-Translator: José Samuel Facundo da Silva <samuel.facundo@ufca.edu.br>\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:18 settings.py:9
msgid "Authentication"
msgstr "Autenticação"

#: forms.py:18
msgid "Email"
msgstr "E-mail"

#: forms.py:21
msgid "Password"
msgstr "Senha"

#: forms.py:23 forms.py:72
msgid "Remember me"
msgstr "Lembrar de mim"

#: forms.py:26
msgid ""
"Please enter a correct email and password. Note that the password field is "
"case-sensitive."
msgstr "Por favor, indique um e-mail e senha corretamente. Note que o campo de senha diferencia maiúsculas de minúsculas."

#: forms.py:28
msgid "This account is inactive."
msgstr "Esta conta está inativa."

#: links.py:19
msgid "Logout"
msgstr "Sair"

#: links.py:23
msgid "Change password"
msgstr "Alterar a senha"

#: settings.py:13
msgid ""
"Controls the mechanism used to authenticated user. Options are: username, "
"email"
msgstr "Controla o mecanismo usado para autenticar o usuário. As opções são: nome de usuário, e-mail"

#: settings.py:20
msgid ""
"Maximum time an user clicking the \"Remember me\" checkbox will remain "
"logged in. Value is time in seconds."
msgstr "Tempo máximo que um usuário que tenha clicado na caixa de seleção \"Lembrar de mim\" permanecerá logado. O valor corresponde ao tempo em segundos."

#: views.py:79
msgid "Current user password change"
msgstr "Alteração de senha do usuário atual"

#: views.py:84
msgid "Changing the password is not allowed for this account."
msgstr "Alterar a senha não é permitido para esta conta."

#: views.py:101
msgid "Your password has been successfully changed."
msgstr "Sua senha foi alterada com sucesso"
