# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# yulin Gong <540538248@qq.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:30-0400\n"
"PO-Revision-Date: 2019-02-02 02:48+0000\n"
"Last-Translator: yulin Gong <540538248@qq.com>\n"
"Language-Team: Chinese (http://www.transifex.com/rosarior/mayan-edms/language/zh/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: apps.py:48 permissions.py:8 settings.py:10
msgid "Document signatures"
msgstr "文档签名"

#: apps.py:88
msgid "Date"
msgstr "日期"

#: apps.py:91 models.py:46
msgid "Key ID"
msgstr "密钥ID"

#: apps.py:95 forms.py:64 models.py:50
msgid "Signature ID"
msgstr "签名ID"

#: apps.py:96 forms.py:76
msgid "None"
msgstr "没有"

#: apps.py:99
msgid "Type"
msgstr "类型"

#: forms.py:21
msgid "Key"
msgstr "密钥"

#: forms.py:25
msgid "Passphrase"
msgstr "密码"

#: forms.py:46
msgid "Signature is embedded?"
msgstr "签名是嵌入式的吗？"

#: forms.py:48
msgid "Signature date"
msgstr "签名日期"

#: forms.py:51
msgid "Signature key ID"
msgstr "签名密钥ID"

#: forms.py:53
msgid "Signature key present?"
msgstr "签名密钥存在？"

#: forms.py:66
msgid "Key fingerprint"
msgstr "密钥指纹"

#: forms.py:70
msgid "Key creation date"
msgstr "密钥创建日期"

#: forms.py:75
msgid "Key expiration date"
msgstr "密钥到期日期"

#: forms.py:80
msgid "Key length"
msgstr "密钥长度"

#: forms.py:84
msgid "Key algorithm"
msgstr "密钥算法"

#: forms.py:88
msgid "Key user ID"
msgstr "密钥用户ID"

#: forms.py:92
msgid "Key type"
msgstr "密钥类型"

#: links.py:36
msgid "Verify all documents"
msgstr "验证所有文档"

#: links.py:43 links.py:61 queues.py:8
msgid "Signatures"
msgstr "签名"

#: links.py:49
msgid "Delete"
msgstr "删除"

#: links.py:54
msgid "Details"
msgstr "细节"

#: links.py:67
msgid "Download"
msgstr "下载"

#: links.py:73
msgid "Upload signature"
msgstr "上传签名"

#: links.py:79
msgid "Sign detached"
msgstr "分离签署"

#: links.py:85
msgid "Sign embedded"
msgstr "嵌入签署"

#: models.py:40
msgid "Document version"
msgstr "文件版本"

#: models.py:44
msgid "Date signed"
msgstr "签署日期"

#: models.py:54
msgid "Public key fingerprint"
msgstr "公钥指纹"

#: models.py:60
msgid "Document version signature"
msgstr "文档版本签名"

#: models.py:61
msgid "Document version signatures"
msgstr "文档版本签名"

#: models.py:80
msgid "Detached"
msgstr "分离的"

#: models.py:82
msgid "Embedded"
msgstr "嵌入的"

#: models.py:97
msgid "Document version embedded signature"
msgstr "文档版嵌入签名"

#: models.py:98
msgid "Document version embedded signatures"
msgstr "文档版嵌入签名"

#: models.py:131
msgid "Signature file"
msgstr "签名文件"

#: models.py:138
msgid "Document version detached signature"
msgstr "文档版本分离签名"

#: models.py:139
msgid "Document version detached signatures"
msgstr "文档版本分离签名"

#: models.py:142
msgid "signature"
msgstr "签名"

#: permissions.py:13
msgid "Sign documents with detached signatures"
msgstr "签署带有分离签名的文档"

#: permissions.py:17
msgid "Sign documents with embedded signatures"
msgstr "签署带有嵌入签名的文档"

#: permissions.py:21
msgid "Delete detached signatures"
msgstr "删除分离签名"

#: permissions.py:25
msgid "Download detached document signatures"
msgstr "下载分离的文档签名"

#: permissions.py:29
msgid "Upload detached document signatures"
msgstr "上传分离的文档签名"

#: permissions.py:33
msgid "Verify document signatures"
msgstr "验证文档签名"

#: permissions.py:37
msgid "View details of document signatures"
msgstr "查看文档签名的详情"

#: queues.py:11
msgid "Verify key signatures"
msgstr "验证密钥签名"

#: queues.py:15
msgid "Unverify key signatures"
msgstr "取消验证密钥签名"

#: queues.py:19
msgid "Verify document version"
msgstr "验证文档版本"

#: queues.py:24
msgid "Verify missing embedded signature"
msgstr "验证缺少的嵌入签名"

#: settings.py:14
msgid "Path to the Storage subclass to use when storing detached signatures."
msgstr "存储分离签名时要使用的存储子类的路径。"

#: settings.py:23
msgid "Arguments to pass to the SIGNATURE_STORAGE_BACKEND. "
msgstr "传递给SIGNATURE_STORAGE_BACKEND的参数。"

#: views.py:68 views.py:161
msgid "Passphrase is needed to unlock this key."
msgstr "解密此密钥需要的密码。"

#: views.py:78 views.py:171
msgid "Passphrase is incorrect."
msgstr "密码不正确。"

#: views.py:99 views.py:191
msgid "Document version signed successfully."
msgstr "文档版本已成功签名。"

#: views.py:123
#, python-format
msgid "Sign document version \"%s\" with a detached signature"
msgstr "以分离签名签署文档版本“%s”"

#: views.py:222
#, python-format
msgid "Sign document version \"%s\" with a embedded signature"
msgstr "以嵌入签名签署文档版本“%s”"

#: views.py:245
#, python-format
msgid "Delete detached signature: %s"
msgstr "删除分离签名：%s"

#: views.py:266
#, python-format
msgid "Details for signature: %s"
msgstr "签名详情：%s"

#: views.py:306
msgid ""
"Signatures help provide authorship evidence and tamper detection. They are "
"very secure and hard to forge. A signature can be embedded as part of the "
"document itself or uploaded as a separate file."
msgstr "签名有助于提供作者证据和篡改检测。它们非常安全且难以伪造。签名可以作为文档本身的一部分嵌入，也可以作为单独的文件上传。"

#: views.py:328
msgid "There are no signatures for this document."
msgstr "此文档没有签名。"

#: views.py:331
#, python-format
msgid "Signatures for document version: %s"
msgstr "文档版本%s的签名"

#: views.py:360
#, python-format
msgid "Upload detached signature for document version: %s"
msgstr "上传文档版本%s的分离签名"

#: views.py:377
msgid "On large databases this operation may take some time to execute."
msgstr "在大型数据库上，此操作可能需要一些时间才能执行。"

#: views.py:378
msgid "Verify all document for signatures?"
msgstr "验证签名的所有文档？"

#: views.py:388
msgid "Signature verification queued successfully."
msgstr "签名验证成功排队。"
